from django.apps import AppConfig


class WebsitetemplateConfig(AppConfig):
    name = 'websitetemplate'
