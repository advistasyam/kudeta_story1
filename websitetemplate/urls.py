from django.urls import path
from . import views

app_name = 'websitetemplate'

urlpatterns = [ 
    path('', views.landing, name='landing'),
]

